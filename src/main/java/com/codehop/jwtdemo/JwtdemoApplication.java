package com.codehop.jwtdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtdemoApplication {

	//This is the main method
	public static void main(String[] args) {
		SpringApplication.run(JwtdemoApplication.class, args);
	}

}
