package com.codehop.jwtdemo.authentication;

import com.codehop.jwtdemo.entity.Users;
import com.codehop.jwtdemo.jwt.JwtUtils;
import com.codehop.jwtdemo.model.AuthLoginRequest;
import com.codehop.jwtdemo.model.AuthResponse;
import com.codehop.jwtdemo.model.AuthSignupRequest;
import com.codehop.jwtdemo.model.Status;
import com.codehop.jwtdemo.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class AuthService {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    PasswordEncoder passwordEncoder;

    public ResponseEntity<AuthResponse> login(AuthLoginRequest authLoginRequest) {
        System.out.println("Login method called");
        Authentication authenticatedUser;
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(authLoginRequest.getEmail(), authLoginRequest.getPassword());

        try{
            authenticatedUser =  authenticationManager.authenticate(authenticationToken);
        }
        catch (Exception e){
            AuthResponse authResponse = new AuthResponse();
            authResponse.setAccess_token(null);
            authResponse.setEmail(authLoginRequest.getEmail());
            authResponse.setStatus(Status.BAD_CREDENTIALS);
            return new ResponseEntity<>(authResponse, HttpStatus.NOT_FOUND);
        }

        Users user = (Users)authenticatedUser.getPrincipal();
//        Users user1 = usersRepository.findByEmail(authLoginRequest.getEmail()).get();

        String accessToken = jwtUtils.generateToken(user);

        AuthResponse authResponse = new AuthResponse();
        authResponse.setAccess_token(accessToken);
        authResponse.setEmail(user.getEmail());
        authResponse.setRole(user.getRole());
        authResponse.setStatus(Status.LOGIN_SUCCESSFULLY);

        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }



    public ResponseEntity<AuthResponse> signup(AuthSignupRequest authSignupRequest) {

        Users user = Users.builder()
                .name(authSignupRequest.getName())
                .email(authSignupRequest.getEmail())
                .password(passwordEncoder.encode(authSignupRequest.getPassword()))
                .role(authSignupRequest.getRole())
                .build();

        if(usersRepository.findByEmail(authSignupRequest.getEmail()).isPresent()){
            AuthResponse authResponse = new AuthResponse();
            authResponse.setAccess_token(null);
            authResponse.setEmail(authSignupRequest.getEmail());
            authResponse.setRole(null);
            authResponse.setStatus(Status.USER_ALREADY_EXIST);
            return new ResponseEntity<>(authResponse, HttpStatus.ALREADY_REPORTED);
        }

        Users savedUser = usersRepository.save(user);

        String accessToken = jwtUtils.generateToken(user);

        AuthResponse authResponse = new AuthResponse();
        authResponse.setAccess_token(accessToken);
        authResponse.setEmail(authSignupRequest.getEmail());
        authResponse.setRole(authSignupRequest.getRole());
        authResponse.setStatus(Status.USER_REGISTER_SUCCESSFULLY);

        return new ResponseEntity<>(authResponse, HttpStatus.OK);

    }


}
