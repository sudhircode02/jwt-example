package com.codehop.jwtdemo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
@PreAuthorize("hasrole('ADMIN')")
public class AdminController {

    @GetMapping("/home")
    public ResponseEntity<?> adminHome(){
        return ResponseEntity.ok("Admin Home Page");
    }


}
