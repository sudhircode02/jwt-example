package com.codehop.jwtdemo.controller;


import com.codehop.jwtdemo.authentication.AuthService;
import com.codehop.jwtdemo.model.AuthLoginRequest;
import com.codehop.jwtdemo.model.AuthResponse;
import com.codehop.jwtdemo.model.AuthSignupRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;


    @PostMapping("/login")
    public ResponseEntity<AuthResponse> login(@RequestBody AuthLoginRequest authLoginRequest){
        return authService.login(authLoginRequest);
    }


    @PostMapping("/signup")
    public ResponseEntity<AuthResponse> signup(@RequestBody AuthSignupRequest authSignupRequest){
        return authService.signup(authSignupRequest);
    }

}
