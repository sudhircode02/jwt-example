package com.codehop.jwtdemo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/intern")
//@PreAuthorize("hasrole('INTERN')")
public class InternController {


    @GetMapping("/home")
    public ResponseEntity<?> internHome(){
        return ResponseEntity.ok("Intern Home Page");
    }
}
