package com.codehop.jwtdemo.jwt;

import com.codehop.jwtdemo.entity.Users;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.*;
import java.util.function.Function;

@Service
public class JwtUtils {

    private Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${spring.app.jwtSecret}")
    private String SECRET;

    @Value("${spring.app.jwtExpiration}")
    private int TOKEN_EXPIRE_TIME ;

    // Below method are used by AuthController
    public String generateToken(UserDetails userDetails) {

        String jwtToken = Jwts.builder()
                //The "sub" (subject) claim is a string that identifies the principal that is the subject of the JWT
                .subject(userDetails.getUsername())

                // Setting the Claims of users
                .claim("authorities", populateAuthorities(userDetails.getAuthorities()))

                //The "iat" (issued at) claim is a timestamp that indicates the time at which the JWT was issued
                .issuedAt(new Date(System.currentTimeMillis()))

                //The claim identifies the expiration time on or after which the JWT MUST NOT be accepted for processing
                .expiration(new Date(System.currentTimeMillis() + TOKEN_EXPIRE_TIME))

                //signWith() method to help sign a JWT with a specific cryptographic algorithm and a secret key
                .signWith(getSigninKey(), SignatureAlgorithm.HS256)
                //.signWith(akey, Jwts. SIG. HS512)

                // similar to built()
                .compact();

        return jwtToken;
    }

    private String populateAuthorities(Collection<? extends GrantedAuthority> authorities) {
        Set<String> authoritiesSet = new HashSet<>();
        for(GrantedAuthority authority : authorities){
            authoritiesSet.add(authority.getAuthority());
        }
        return String.join(",", authoritiesSet);
    }

    private Key getSigninKey(){
        byte[] keyByte = Decoders.BASE64.decode(SECRET);
        return Keys.hmacShaKeyFor(keyByte);
        //return Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
    }



    // Below method are used by AuthTokenFilter

    public String getTokenFromHeader(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if(bearerToken != null && bearerToken.startsWith("Bearer ")){
            System.out.println(bearerToken);
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(String token) {
        try{
            System.out.println("Validate JWT token");
            Jwts.parser()
                    .verifyWith((SecretKey) getSigninKey())
                    .build()
                    .parseSignedClaims(token);
            System.out.println("validate jwt complete");
            return true;
        }
        catch (MalformedJwtException e){
            logger.error("Invalid JWT token : {}", e.getMessage());
        }
        catch (ExpiredJwtException e){
            logger.error("JWT token is Expired: {}", e.getMessage());
        }
        catch (UnsupportedJwtException e){
            logger.error("JWT token unsupported: {}", e.getMessage());
        }
        catch (IllegalArgumentException e) {
            logger.error("JWT Claims string is empty : {}", e.getMessage());
        }
        return false;
    }

    public String getUsernameFromToken(String token) {
        return Jwts.parser()
                .verifyWith((SecretKey) getSigninKey())
                .build()
                .parseSignedClaims(token)
                .getPayload().getSubject();
    }

}








