package com.codehop.jwtdemo.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthLoginRequest {

    private String email;
    private String password;
}
