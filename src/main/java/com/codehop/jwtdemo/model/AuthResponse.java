package com.codehop.jwtdemo.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthResponse {

    private String email;
    private String access_token;
    private Role role;
    private Status status;
}
