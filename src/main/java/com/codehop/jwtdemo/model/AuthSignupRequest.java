package com.codehop.jwtdemo.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthSignupRequest {

    private String name;
    private String email;
    private String password;
    private Role role;
}
