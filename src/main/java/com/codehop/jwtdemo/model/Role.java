package com.codehop.jwtdemo.model;

public enum Role {

    ADMIN ,

    INTERN,

    USER
}
