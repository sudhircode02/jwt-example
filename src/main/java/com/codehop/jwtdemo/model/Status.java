package com.codehop.jwtdemo.model;

public enum Status {

    USER_ALREADY_EXIST,
    USER_REGISTER_SUCCESSFULLY,
    BAD_CREDENTIALS,
    LOGIN_SUCCESSFULLY
}
