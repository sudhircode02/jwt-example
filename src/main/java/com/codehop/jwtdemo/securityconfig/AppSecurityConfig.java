package com.codehop.jwtdemo.securityconfig;

import com.codehop.jwtdemo.authentication.AuthTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class AppSecurityConfig {

    @Autowired
    AuthEntryPoint authEntryPoint;
    @Autowired
    AuthTokenFilter authTokenFilter;

    @Autowired
    AuthenticationProvider authenticationProvider;


    @Bean
    SecurityFilterChain customSecurityFilterChain(HttpSecurity http) throws Exception {
        System.out.println("Custom Security Filter chain is Called");

        // disable cors & csrf
        http.cors(AbstractHttpConfigurer::disable);
        http.csrf(AbstractHttpConfigurer::disable);

        // Making Endpoint Authernticated
        http.authorizeHttpRequests(
                req -> req
                        .requestMatchers("/auth/**").permitAll()
                        .requestMatchers("/admin/**") .hasAnyRole("ADMIN")
                        .requestMatchers("/intern/**").hasAnyRole("ADMIN","INTERN")
                        .requestMatchers("/user/**")  .hasAnyRole("ADMIN","INTERN","USER")
                        .anyRequest().authenticated()
        );

        // Setting Session Management to StateLess
        http.sessionManagement(
                session -> session
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        );

        //Setting the AuthEntryPoint to handle the Exception
        http.exceptionHandling(
                exception -> exception
                        .authenticationEntryPoint(authEntryPoint)
        );


        // Adding AuthEntryFilter to FilterChain
        http.addFilterBefore(authTokenFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}
